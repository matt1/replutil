package org.matt1.replutil;

import java.io.IOException;
import java.util.logging.Logger;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import org.matt1.replutil.gui.ReplUtilController;
import org.matt1.replutil.script.ScriptManager;


/**
 * JavaFX Application for the GUI app.  
 * 
 * @author matt
 *
 */
public class ReplUtilApplication extends Application {
	
	/** Default logger */
	private static Logger log  = Logger.getLogger(ReplUtilApplication.class.getName());
	
	@Override
	public void start(Stage stage) throws IOException {
		log.info("Starting Application...");
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("./gui/ReplUtilView.fxml"));
        Parent root = fxmlLoader.load();
        ReplUtilController controller = (ReplUtilController) fxmlLoader.getController();
        fxmlLoader.setRoot(root);
        
        // Set the script manager
        ScriptManager scriptManager = new ScriptManager();
        controller.setScriptManager(scriptManager);
		
        // Set the scene
        Scene scene = new Scene(root);		     
        stage.setTitle("REPL Util");
        stage.setScene(scene);
        stage.show();
        
        controller.setStage(stage);
        
        scene.getRoot().requestFocus();
	}

	public static void main(String[] args) {
		launch(args);
	}

}





