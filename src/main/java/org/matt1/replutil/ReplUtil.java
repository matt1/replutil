package org.matt1.replutil;

import java.io.PrintStream;
import java.security.Security;
import java.util.logging.Logger;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

/**
 * Main class - basically just starts the UI
 * 
 * @author matt
 *
 */
public class ReplUtil {
	
	/** Default output stream to use for output */
	PrintStream output = System.out;
	
	/** Default logger */
	private static Logger log  = Logger.getLogger(ReplUtil.class.getName());
	
	/**
	 * Create a new instance of ReplUtil
	 */
	public ReplUtil() {
		// Register BouncyCastle provider
		Security.addProvider(new BouncyCastleProvider());
	}
	
	/**
	 * Creates a new instance and initialises the application
	 * 
	 * @param arguments
	 */
	public static void main(String[] arguments) {
		ReplUtil totalHashOfIt = new ReplUtil();
		totalHashOfIt.init(arguments);
	}
	
	/**
	 * Initialise the application - basically just starts the GUI for now
	 * 
	 * @param arguments
	 */
	public void init(String[] arguments) {
							
		// Register BouncyCastle provider before kicking off either GUI or cmd
		Security.addProvider(new BouncyCastleProvider());
		
		gui(arguments);
		
	}
	
	/**
	 * Kick off the GUI
	 */
	private void gui(String[] arguments) {
		ReplUtilApplication.launch(ReplUtilApplication.class, arguments);
	}
	
	
	/**
	 * Set an output stream for use by the default output
	 * 
	 * @param out
	 */
	public void setOutputStream(PrintStream out) {
		output = out;
	}
}
