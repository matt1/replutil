package org.matt1.replutil.model;

import java.io.File;

/**
 * Encapsulates the string import settings for passing between dialog and app
 * 
 * @author Matt
 *
 */
public class ImportSettings {

	/** File to load data from */
	private File file;
	
	/** What type of import we're dealing with */
	public enum ImportType {
		STRING,
		ARRAY,
		DELIMITED,
		JSON
	}
	
	/** Type of import */
	private ImportType type;
	
	/** Delimiter to use */
	private String delimiter;
	
	/** Destination variable name */
	private String destination;
	
	public ImportSettings(File file, ImportType type, String destination) {
		this(file, type, destination, null);
	}
	
	public ImportSettings(File file, ImportType type, String destination, String delimiter) {
		this.file = file;
		this.type = type;
		this.delimiter = delimiter;
		this.destination = destination;
	}

	public File getFile() {
		return file;
	}
	
	public String getFilePath() {
		return file.getAbsolutePath().replace("\\", "\\\\");
	}

	public ImportType getType() {
		return type;
	}

	public String getDelimiter() {
		return delimiter;
	}
	
	public String getDestination() {
		return this.destination;
	}
	
	
	
}
