package org.matt1.replutil.model;

import java.io.File;

/**
 * Encapsulates the string export settings for passing between dialog and app
 * 
 * @author Matt
 *
 */
public class ExportSettings {

	/** File to load data from */
	private File file;
	
	/** Destination variable name */
	private String source;
	
	public ExportSettings(File file, String source) {
		this.file = file;
		this.source = source;
	}
	
	public File getFile() {
		return file;
	}
	
	public String getFilePath() {
		return file.getAbsolutePath().replace("\\", "\\\\");
	}
	
	public String getSource() {
		return this.source;
	}
	
	
	
}
