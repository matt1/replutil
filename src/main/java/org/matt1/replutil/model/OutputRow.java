package org.matt1.replutil.model;

/**
 * Contains data about each output row.
 * 
 * @author Matt
 *
 */
public class OutputRow {

	/** The type of command */
	public enum Type {
		COMMAND,
		RESULT, 
		ERROR,
		CONSOLE_LOG,
		CONSOLE_WARN,
		CONSOLE_ERROR
	};
	
	/** Type of row this is */
	private Type rowType;
	
	/** Contains the content to be displayed */
	private Object content;
	
	public OutputRow(Object content, Type type) {
		this.content = content;
		this.rowType = type;
	}

	public Type getType() {
		return rowType;
	}

	public Object getContent() {
		return content;
	}

}
