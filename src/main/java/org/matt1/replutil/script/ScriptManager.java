package org.matt1.replutil.script;

import javax.script.Bindings;
import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptException;

import jdk.nashorn.api.scripting.NashornScriptEngineFactory;

/**
 * Handles the nashorn script instance
 * 
 * @author Matt
 *
 */
public class ScriptManager {

	private ScriptEngine engine;
	
	private Bindings bindings;
	
	public ScriptManager() {
		
		NashornScriptEngineFactory factory = new NashornScriptEngineFactory();
		
		// More info on these options here:
		// https://wiki.openjdk.java.net/display/Nashorn/Nashorn+extensions
		String[] opts = new String[]{
				"-strict", 	// run in strict ECMAScript 5.1 mode
				"-nj", 		// no java support
				"-nse"};	// No special syntax extensions
		engine = factory.getScriptEngine(opts);
			
		// bindings.keySet() will contain all new global variables/functions etc.
		// but does not contain normal globals (e.g. exit) etc.  Could be useful
		this.bindings = engine.getBindings(ScriptContext.ENGINE_SCOPE);

	}
	
	/**
	 * Execute the script
	 * 
	 * @param code
	 * @return
	 * @throws ScriptException 
	 */
	public Object eval(String code) throws ScriptException {
		return engine.eval(code);
	}
	
	/**
	 * Add an object to the script's scope
	 * @param key Name of global to use in javascript 
	 * @param obj Object to use
	 */
	public void add(String key, Object obj) {
		this.engine.put(key, obj);
	}
	
	public Bindings getBindings() {
		return this.bindings;
	}
	
}
