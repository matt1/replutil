package org.matt1.replutil.script;

import java.util.function.BiConsumer;

import org.matt1.replutil.model.OutputRow;
import org.matt1.replutil.model.OutputRow.Type;

/**
 * Console class for javascript
 * 
 * @author Matt
 *
 */
public class ReplConsole {

	BiConsumer<Object, OutputRow.Type> guiConsoleFunction;
	
	/**
	 * Creates a new console object
	 * 
	 * @param guiConsoleFunction Function in controller that handles showing output in the 
	 * GUI's console
	 */
	public ReplConsole(BiConsumer<Object, OutputRow.Type> guiConsoleFunction) {
		this.guiConsoleFunction = guiConsoleFunction;
	}
	
	public void log(String message) {
		this.guiConsoleFunction.accept(message, Type.CONSOLE_LOG);
	}
	
	public void warn(String message) {
		this.guiConsoleFunction.accept(message, Type.CONSOLE_WARN);
	}
	
	public void error(String message) {
		this.guiConsoleFunction.accept(message, Type.CONSOLE_ERROR);
	}
	
}
