package org.matt1.replutil.script.utils;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import javax.script.ScriptException;

import jdk.nashorn.internal.objects.NativeArray;
import jdk.nashorn.internal.objects.NativeJava;

/**
 * Network based utils
 * 
 * @author Matt
 *
 */
public class NetworkUtils {

	/**
	 * Performs a simple DNS lookup
	 * @param url
	 * @return
	 */
	public NativeArray dnsLookup(String url) throws ScriptException {
		try {
			InetAddress[] inetAddressArray = InetAddress.getAllByName(url);
			Object[] wrapper = new Object[inetAddressArray.length];
			for (int i = 0; i<inetAddressArray.length;i++) {
				Map<String,String> map = new HashMap<>();
				map.put("ip", inetAddressArray[i].getHostAddress());
				map.put("canonicalhostname", inetAddressArray[i].getCanonicalHostName());
				map.put("hostname", inetAddressArray[i].getHostName());
				wrapper[i] = map;
			}
			
			return NativeJava.from(null, wrapper);
		} catch (UnknownHostException e) {
			throw new ScriptException("Hostname '" + url + "' was invalid.");
		}		
	}
	
	/**
	 * Performs a reverse DNS lookup
	 * @param ip
	 * @return
	 */
	public String dnsReverseLookup(String ip) throws ScriptException {
		try {
			
			InetAddress inetAddress = InetAddress.getByName(ip);
			
			return inetAddress.getCanonicalHostName();
		} catch (UnknownHostException e) {
			throw new ScriptException("IP '" + ip + "' was invalid.");
		}		
	}
	
	/**
	 * Is the host reachable (default 5 second timeout)
	 * @param host
	 * @return
	 * @throws ScriptException 
	 */
	public Boolean isReachable(String host) throws ScriptException {
		return isReachable(host, 5000);
	}
	
	/**
	 * Is the host reachable?
	 * 
	 * @param host
	 * @param timeout
	 * @return
	 * @throws ScriptException 
	 */
	public Boolean isReachable(String host, int timeout) throws ScriptException {
		try {
			
			InetAddress inetAddress = InetAddress.getByName(host);
			
			return inetAddress.isReachable(timeout);
		} catch (IOException e) {
			throw new ScriptException("host '" + host + "' was invalid.");
		}		
	}
	
}
