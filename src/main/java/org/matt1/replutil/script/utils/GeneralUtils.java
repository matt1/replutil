package org.matt1.replutil.script.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.script.ScriptException;

/**
 * Native class for javascript helper/util functions
 * 
 * @author Matt
 *
 */
public class GeneralUtils{
	
	/** Network utils */
	public NetworkUtils net = new NetworkUtils();
	
	/** File utils */
	public FileUtils io = new FileUtils();
	
	/** Hash utils */
	public HashUtils hash = new HashUtils();
	
	
	/** Get operating system */
	public String getOS() {
		return System.getProperty("os.name");
	}
	
	/** Get host name  of local machine 
	 * @throws ScriptException */
	public String getHostname() throws ScriptException {
		try {
			InetAddress local = InetAddress.getLocalHost();
			return local.getHostName();
		} catch (UnknownHostException e) {
			throw new ScriptException("Unable to get local hostname");
		}
	}
	
	/**
	 * Get address of local machine
	 * 
	 * @return
	 * @throws ScriptException 
	 */
	public String getLocalAddress() throws ScriptException {
		try {
			InetAddress local = InetAddress.getLocalHost();
			return local.getHostAddress();
		} catch (UnknownHostException e) {
			throw new ScriptException("Unable to get local hostname");
		}
	}
	
	/**
	 * Get available CPUs
	 * 
	 * @return
	 */
	public int getCores() {
		return Runtime.getRuntime().availableProcessors();
	}
	
	public void debug() throws ScriptException {
		throw new ScriptException("Error in debug shiv");

	}
	
}
