package org.matt1.replutil.script.utils;

import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.bouncycastle.util.encoders.Hex;

/**
 * Utils for hashing
 * 
 * @author Matt
 *
 */
public class HashUtils {

	/** Provider name for Bouncy Castle" */
	private static final String PROVIDER = "BC";
	
	/** Hash Function */
	public enum HashFunction {

		MD2("MD2"),
		MD4("MD4"),
		MD5("MD5"),
		SHA1("SHA1"),
		SHA224("SHA224"),
		SHA256("SHA256"),
		SHA384("SHA384"),
		SHA512("SHA512"),
		RIPE128("RIPEMD128"),
		RIPE160("RIPEMD160"),
		RIPE256("RIPEMD256"),
		RIPE320("RIPEMD320"),
		TIGER192_3("TIGER192,3"),
		GOST("GOST3411"),
		WHIRLPOOL("WHIRLPOOL");	
		
		private String name;
		
		HashFunction(String name) {
			this.name = name;
		}
		
		/**
		 * Get the function identifier for the hash function
		 */
		public String toString() {
			return this.name;
		}
	}

	
	/**
	 * MD2 Hash
	 * 
	 * @param txt
	 * @return
	 * @throws ScriptException
	 */
	public String md2(String txt) throws ScriptException {
		return hash(txt, HashFunction.MD2);
	}
	
	/**
	 * MD4 Hash
	 * 
	 * @param txt
	 * @return
	 * @throws ScriptException
	 */
	public String md4(String txt) throws ScriptException {
		return hash(txt, HashFunction.MD4);
	}
	
	/**
	 * MD5 Hash
	 * 
	 * @param txt
	 * @return
	 * @throws ScriptException
	 */
	public String md5(String txt) throws ScriptException {
		return hash(txt, HashFunction.MD5);
	}	
	
	/**
	 * SHA1 Hash
	 * 
	 * @param txt
	 * @return
	 * @throws ScriptException
	 */
	public String sha1(String txt) throws ScriptException {
		return hash(txt, HashFunction.SHA1);
	}
	
	/**
	 * SHA224 Hash
	 * 
	 * @param txt
	 * @return
	 * @throws ScriptException
	 */
	public String sha224(String txt) throws ScriptException {
		return hash(txt, HashFunction.SHA224);
	}
	
	/**
	 * SHA256 Hash
	 * 
	 * @param txt
	 * @return
	 * @throws ScriptException
	 */
	public String sha256(String txt) throws ScriptException {
		return hash(txt, HashFunction.SHA256);
	}
	
	/**
	 * SHA384 Hash
	 * 
	 * @param txt
	 * @return
	 * @throws ScriptException
	 */
	public String sha384(String txt) throws ScriptException {
		return hash(txt, HashFunction.SHA384);
	}
	
	/**
	 * RIPE128 Hash
	 * 
	 * @param txt
	 * @return
	 * @throws ScriptException
	 */
	public String ripe128(String txt) throws ScriptException {
		return hash(txt, HashFunction.RIPE128);
	}

	/**
	 * RIPE160 Hash
	 * 
	 * @param txt
	 * @return
	 * @throws ScriptException
	 */
	public String ripe160(String txt) throws ScriptException {
		return hash(txt, HashFunction.RIPE160);
	}
	
	/**
	 * RIPE256 Hash
	 * 
	 * @param txt
	 * @return
	 * @throws ScriptException
	 */
	public String ripe256(String txt) throws ScriptException {
		return hash(txt, HashFunction.RIPE256);
	}
	
	/**
	 * RIPE320 Hash
	 * 
	 * @param txt
	 * @return
	 * @throws ScriptException
	 */
	public String ripe320(String txt) throws ScriptException {
		return hash(txt, HashFunction.RIPE320);
	}
	
	/**
	 * SHA512 Hash
	 * 
	 * @param txt
	 * @return
	 * @throws ScriptException
	 */
	public String sha512(String txt) throws ScriptException {
		return hash(txt, HashFunction.SHA512);
	}
		
	public String hash(byte[] bytes, HashFunction function) throws ScriptException  {
		String hex = null;
		try {
			byte[] d = MessageDigest.getInstance(function.toString(), PROVIDER).digest(bytes);
			hex = Hex.toHexString(d);
		} catch (NoSuchProviderException nspe) {
			throw new ScriptException("No such hash provider " + PROVIDER);
		} catch (NoSuchAlgorithmException nsae) {
			throw new ScriptException("No such hash algorithm '" + function + "'");
		}
		return hex;
	}
	
	public String hash(String string, HashFunction function) throws ScriptException {
		return hash(string.getBytes(), function);

	}

	public String hash(InputStream stream, HashFunction function) throws ScriptException {
		try {
			return hash(IOUtils.toByteArray(stream), function);
		} catch (IOException ioe) {
			throw new ScriptException("IO Exception trying to hash file.");
		}
			
	}
	
}
