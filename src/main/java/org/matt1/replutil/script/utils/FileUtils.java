package org.matt1.replutil.script.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import javax.script.ScriptException;

import jdk.nashorn.internal.objects.NativeArray;
import jdk.nashorn.internal.objects.NativeJava;
import jdk.nashorn.internal.runtime.JSONFunctions;

import org.odftoolkit.simple.SpreadsheetDocument;
import org.odftoolkit.simple.table.Cell;
import org.odftoolkit.simple.table.Table;

/**
 * General file utils
 * 
 * @author Matt
 *
 */
public class FileUtils {


	/**
	 * Save the data string to the filename provided
	 * 
	 * @param filename
	 * @param data
	 * @throws IOException
	 */
	public void saveString(String filename, String data) throws IOException {
		Files.write(Paths.get(filename), data.getBytes(), StandardOpenOption.CREATE);
	}
	
	/**
	 * Loads JSON as an object 
	 * O
	 * @param filename
	 * @return
	 * @throws IOException
	 */
	public Object loadJSON(String filename) throws IOException {
		String json = loadString(filename);
		return JSONFunctions.parse(json, null);
	}
	
	/**
	 * Load a simple string
	 * @param filename
	 * @return
	 * @throws IOException 
	 */
	public String loadString(String filename) throws IOException {
		String data = new String(Files.readAllBytes(Paths.get(filename)));
		return data;
	}
	
	/**
	 * Load a string into an array
	 * @param filename
	 * @return
	 * @throws IOException 
	 */
	public NativeArray loadStringArray(String filename) throws IOException {
		Object[] lines = Files.lines(Paths.get(filename)).toArray();
		return NativeJava.from(null, lines);
	}
	
	/**
	 * Load a CSV into an array of arrays.
	 * @param filename
	 * @return
	 * @throws IOException 
	 */
	public NativeArray loadStringDelimited(String filename, String delimiter) throws IOException {
		
		Object[] lines = Files.lines(Paths.get(filename)).map(line -> line.split(",")).toArray();

		for (int i=0;i<lines.length;i++) {
			lines[i] = NativeJava.from(null, lines[i]);
		}
		
		return NativeJava.from(null, lines);
	}
	
	/**
	 * Return the first sheet of the ODS workbook
	 * @param filename
	 * @return
	 * @throws ScriptException
	 */
	public NativeArray loadODS(String filename) throws ScriptException {
		return loadODS(filename, 0);
	}
	
	/**
	 * Load a ODS file as array
	 * @param filename
	 * @return
	 * @throws ScriptException 
	 */
	public NativeArray loadODS(String filename, int sheetIndex) throws ScriptException {
		try {
			SpreadsheetDocument doc = SpreadsheetDocument.loadDocument(filename);
			Table sheet0 = doc.getSheetByIndex(0);
			
			Object[] rows = new Object[sheet0.getRowCount()];
			for (int row = 0; row<sheet0.getRowCount();row++) {
				int cellCount = sheet0.getRowByIndex(row).getCellCount();
				Object[] cols = new Object[cellCount];
				for (int col = 0;col<cellCount;col++) {
					Cell c = sheet0.getCellByPosition(col, row);
					if (c != null) {
						cols[col] = c.getStringValue();
					}
				}
				rows[row] = NativeJava.from(null, cols);				
			}
			return NativeJava.from(null, rows);
			
		} catch (Exception e) {
			throw new ScriptException("Unable to load ODS");
		}
	}
	
}
