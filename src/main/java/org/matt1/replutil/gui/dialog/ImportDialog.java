package org.matt1.replutil.gui.dialog;

import java.io.File;
import java.util.function.Consumer;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.FileChooser;
import javafx.stage.Window;

import org.controlsfx.control.ButtonBar;
import org.controlsfx.control.ButtonBar.ButtonType;
import org.controlsfx.control.action.AbstractAction;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.DialogStyle;
import org.controlsfx.validation.ValidationSupport;
import org.matt1.replutil.model.ImportSettings;
import org.matt1.replutil.model.ImportSettings.ImportType;


/**
 * Import dialog
 * 
 * @author matt
 *
 */
public class ImportDialog {

	/** Window owner */
	Window owner;
	
	/** Callback to use once the dialog completes */
	Consumer<ImportSettings> callback;
	
	/** The text field for file we're importing */
	final TextField source = new TextField();
	
	/** Actual file we've selected */
	File sourceFile;
	
	/** Browse for file button */
	final Button browse = new Button();
	
	/** Filter type */
	ObservableList<String> options = 
		    FXCollections.observableArrayList(
		        "String", "String-as-Array", "JSON", "Delimited"
		    );
	final ComboBox<String> importType = new ComboBox<String>(options);
	
	/** The text field for the delimiter */
	final TextField delimiter = new TextField();

	/** The text field for the destination variable */
	final TextField destination = new TextField();
		
	/** Validation support */
	ValidationSupport validationSupport = new ValidationSupport();
	
	/** Handler for "ok" */
	final AbstractAction actionStart = new AbstractAction("Import") {
		{
			ButtonBar.setType(this, ButtonType.OK_DONE);
		}
		
		@Override
		public void handle(ActionEvent event) {
	          Dialog dialog = (Dialog) event.getSource();
	          
	          ImportType type = ImportType.STRING;
	          if (importType.getValue().equals("String-as-Array")) {
	        	  type = ImportType.ARRAY;
	          } else if (importType.getValue().equals("JSON")) {
	        	  type = ImportType.JSON;
	          } else if (importType.getValue().equals("String")) {
	        	  type = ImportType.STRING;
	          } else {
	        	  type = ImportType.DELIMITED;
	          }
	          
	          String delimiterValue = null;
	          if (type == ImportType.DELIMITED) {
	        	  delimiterValue = delimiter.getText();
	          }
	          
	          ImportSettings importSettings = new ImportSettings(sourceFile, type, 
	        			  destination.getText(), delimiterValue);
	        
	          Platform.runLater(() -> {
	        	  callback.accept(importSettings);
	          });
	          
	          dialog.hide();			
		}
		
	};
	
	/**
	 * Creates a new Crawler dialog
	 * 
	 * @param owner
	 * @param callback
	 */
	public ImportDialog(Window owner, Consumer<ImportSettings> callback) {
		this.owner = owner;
		this.callback = callback;
		
		browse.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
				FileChooser chooser = new FileChooser();
				chooser.setTitle("Select file to import");
				sourceFile = chooser.showOpenDialog(owner);
				source.setText(sourceFile.getAbsolutePath());								
			}
		});
		
		importType.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if (importType.getValue().equals("Delimited")) {
					delimiter.setDisable(false);
				} else {
					delimiter.setDisable(true);
				}
				
			}
		});
		
		setupValidation();

	}

	private void setupValidation() {

	}
	
	 /**
	  * Show the dialog
	  * 
	  * @param owner
	  */
	 public void show() {
		 Dialog dialog = new Dialog(owner, "Import Data", false, DialogStyle.NATIVE);
	       

	     // layout a custom GridPane containing the input fields and labels
	     final GridPane content = new GridPane();
	     content.setHgap(10);
	     content.setVgap(10);
	       
	     int row = 0;
	     
	     // source
	     final HBox fileLoad = new HBox(8);
	     fileLoad.getChildren().addAll(source, browse);
	     browse.setText("...");
	     content.add(new Label("Source file"), 0, row);
	     content.add(fileLoad, 1, row);
	     source.setTooltip(new Tooltip("The file from where we're importing the data."));
	     source.setPrefWidth(240);
	     GridPane.setHgrow(fileLoad, Priority.ALWAYS);	    
	     
	     // filter pattern
	     importType.setValue("String");
	     importType.setPrefWidth(275);
	     content.add(new Label("Import Type"), 0, ++row);
	     content.add(importType, 1, row);
	     importType.setTooltip(new Tooltip("The type of the import required.\n\nString loads the entire file as a single variable.\nString-as-Array loads the entire file, with each line of the file being a separate element in an array.\nDelimited loads character delimited files (e.g. CSV, TSV) into a multi-dimensional array.\nJSON loads a JSON string into an object."));
	     GridPane.setHgrow(importType, Priority.ALWAYS);
	     
	     // max results
	     content.add(new Label("Delimiter"), 0, ++row);
	     content.add(delimiter, 1, row);
	     delimiter.setAlignment(Pos.CENTER);
	     delimiter.setTooltip(new Tooltip("The delimiter to use when parsing the character delimited input, e.g. a comma for CSV or \\t for TSV."));
	     GridPane.setHgrow(delimiter, Priority.NEVER);
	     
	     // max threads
	     content.add(new Label("Destination variable"), 0, ++row);
	     content.add(destination, 1, row);
	     destination.setAlignment(Pos.CENTER);
	     destination.setTooltip(new Tooltip("The variable into which the data will be loaded."));
	     GridPane.setHgrow(destination, Priority.NEVER);
	     
	     // create the dialog with a custom graphic and the gridpane above as the
	     // main content region
	     dialog.setResizable(false);
	     dialog.setIconifiable(false);
	     dialog.setContent(content);
	     dialog.getActions().addAll(actionStart, Dialog.Actions.CANCEL);
	       
	     // Focus start URL immediately
	     Platform.runLater(() -> {
	     	browse.requestFocus();
	     });

	     
	     dialog.show();
	 }
	 
	 
	
}

