package org.matt1.replutil.gui;


import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.Vector;
import java.util.function.Consumer;
import java.util.logging.Logger;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import javax.script.Bindings;
import javax.script.ScriptException;

import org.matt1.replutil.gui.dialog.ExportDialog;
import org.matt1.replutil.gui.dialog.ImportDialog;
import org.matt1.replutil.model.ExportSettings;
import org.matt1.replutil.model.ImportSettings;
import org.matt1.replutil.model.OutputRow;
import org.matt1.replutil.model.OutputRow.Type;
import org.matt1.replutil.script.ReplConsole;
import org.matt1.replutil.script.ScriptManager;
import org.matt1.replutil.script.utils.GeneralUtils;
import org.matt1.replutil.script.utils.NetworkUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;



/**
 * GUI for replutil
 * 
 * @author matt
 *
 */
public class ReplUtilController implements Initializable {
	
	/** Default logger */
	private static Logger log = Logger.getLogger(ReplUtilController.class.getName());
	
	/** Script manager */
	private ScriptManager script;
	
	/** Bindings for controls */
	@FXML Parent root;
	
	/** menus */
	@FXML MenuItem menuExit;
	@FXML MenuItem menuAbout;
	
	/** Input and output areas */
	@FXML TextArea input;
	@FXML WebView output;

	/** Export button */
	@FXML Button exportButton;
	
	/** Import menu */
	@FXML MenuItem importString;
	@FXML MenuItem importStringArray;
	@FXML MenuItem importDelimited;
	@FXML MenuItem importJson;
	
	/** Contains all previous commands */
	List<String> buffer = new ArrayList<>();
	
	/** Contains all output rows */
	List<OutputRow> outputRows = new Vector<>();
	
	/** Pointer to position in buffer */
	int bufferPointer = 0;
	
	/** Maximum commands to store in the input buffer */
	int MAX_INPUT_BUFFER_LENGTH = 100;
	
	/** Maximum rows to store in the output buffer */
	int MAX_OUTPUT_BUFFER_LENGTH = 500;
	
	/** Stage */
	private Stage stage;
	
	/**
	 * Called by JavaFX when initialisation of this controller is complete
	 */
	@Override
	public void initialize(URL fxmlFileLocation, ResourceBundle resource) {
			
		setupListeners();
		
		// setup webview
		String path = this.getClass().getResource("./output.html").toExternalForm();	
		output.getEngine().load(path);

	}
		
	/**
	 * Setup the global objects for javascript use
	 */
	private void setupObjects() {		
		GeneralUtils utils = new GeneralUtils();	
		ReplConsole console = new ReplConsole(this::output);
		NetworkUtils networkUtils = new NetworkUtils();
		this.script.add("utils", utils);
		//this.script.add("utils.net", networkUtils);
		this.script.add("console", console);
	}
	
	/**
	 * Set the script manager that we're using to execute scripts
	 * 
	 * @param scriptManager
	 */
	public void setScriptManager(ScriptManager scriptManager) {
		this.script = scriptManager;
		setupObjects();
	}
	
	/**
	 * Set the stage and initialise Drag & drop
	 */
	public void setStage(Stage stage) {
		this.stage = stage;
	}

	/**
	 * Export data
	 * @param settings
	 */
	public void exportData(ExportSettings settings) {
		
	}
	
	/**
	 * Import the data
	 * @param settings
	 */
	public void importData(ImportSettings settings) {
		
		String command = null;
		switch (settings.getType()) {
			case ARRAY:
				command = String.format("var %s = utils.io.loadStringArray('%s');", 
						settings.getDestination(), 
						settings.getFilePath());
				break;
			case STRING:
				command = String.format("var %s = utils.io.loadString('%s');", 
						settings.getDestination(), 
						settings.getFilePath());
				break;
			case JSON:
				command = String.format("var %s = utils.io.loadJSON('%s');",
						settings.getDestination(),
						settings.getFilePath());
				break;
			case DELIMITED:
				command = String.format("var %s = utils.loadStringDelimited('%s','%s');", 
						settings.getDestination(), 
						settings.getFilePath(),
						settings.getDelimiter());
				break;
		}
		

		
		try {
			execute(command);
		} catch (ScriptException e) {
			this.outputRows.add(new OutputRow(e, OutputRow.Type.ERROR));
			refreshConsole();
		}
		
	}
	
	/**
	 * Sets up the listeners for the various bits of gui
	 */
	public void setupListeners() {
		
		// menu items
		menuExit.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				System.exit(0);
				
			}
		});
		
		// export data button
		Consumer<ExportSettings> exportConsumer = this::exportData;
		exportButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				new ExportDialog(stage.getOwner(), exportConsumer).show();		
				
			}
		});
		
		// Import data button
		Consumer<ImportSettings> importConsumer = this::importData;
		EventHandler<ActionEvent> importHandler = new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				new ImportDialog(stage.getOwner(), importConsumer).show();				
			}
		};
		
		importString.setOnAction(importHandler);
		importStringArray.setOnAction(importHandler);
		importJson.setOnAction(importHandler);
		importDelimited.setOnAction(importHandler);
		
		// Handle main code input
		input.setOnKeyPressed(new EventHandler<KeyEvent>() {

	        @Override
	        public void handle(KeyEvent evt) {

	        	if (evt.getCode() == KeyCode.ENTER) {
	        		evt.consume();
		        	try {
		    	    	String command = input.getText().trim();
		    	    	Object result = execute(command);
		    	        output(result);
		        	} catch (Exception e) {
		        		outputRows.add(new OutputRow(e, OutputRow.Type.ERROR));
		    			refreshConsole();
		        	} finally {
		    	        input.clear();
		        	}
		        	
	        	} else if (evt.getCode() == KeyCode.UP) {
	        		
	        		input.setText(buffer.get(bufferPointer));
	        		input.positionCaret(input.getText().length());
	        			        		
	        		bufferPointer++;
	        		if (bufferPointer > buffer.size()-1) {
	        			bufferPointer = buffer.size()-1;	        			
	        		}
	        	}  else if (evt.getCode() == KeyCode.DOWN) {
	        		bufferPointer--;
	        		if (bufferPointer < 0) {
	        			bufferPointer = 0;	        			
	        		}
	        		input.setText(buffer.get(bufferPointer));
	        		input.positionCaret(input.getText().length());     		
	        		
	        	}  

	        }
	    });
	}

	/**
	 * Log a result output message
	 * 
	 * @param message
	 */
	private void output(Object message) {
		output(message, OutputRow.Type.RESULT);		
	}
	
	/**
	 * Log an output message of any type
	 * 
	 * @param message
	 * @param type
	 */
	private void output(Object message, OutputRow.Type type) {
		String msg = "undefined";

		if (message != null) {
			msg = message.toString();
		} 
		this.outputRows.add(new OutputRow(message, type));
		refreshConsole();
	}
	
	
	/**
	 * echo the command 
	 * @param message
	 */
	private void echoCommand(String message) {
		this.outputRows.add(new OutputRow(message, OutputRow.Type.COMMAND));
		refreshConsole();
	}
	
	/**
	 * Refresh the UI and append last message to the console
	 * 
	 */
	private void refreshConsole() {
		OutputRow message = this.outputRows.get(this.outputRows.size()-1);
		
		String m = "";
		String c = "command";
		
		if (message.getType() == Type.RESULT) {
			c = "result";
		} else if (message.getType() == Type.CONSOLE_LOG) {
			c = "plain";
		} else if (message.getType() == Type.ERROR || message.getType() == Type.CONSOLE_ERROR) {
			c = "error";
		} else if (message.getType() == Type.CONSOLE_WARN) {
			c = "warn";
		}
		
		if (message.getContent() instanceof Exception) {
			Exception e = (Exception) message.getContent();
			m += e.getMessage();
		} else {
			if (message.getContent() == null) {
				m += "undefined";
			} else {
				m += message.getContent().toString();
			}
		}
		
		WebEngine engine = output.getEngine();
		Document dom = engine.getDocument();
		Element div = dom.createElement("div");
		div.setAttribute("class", c);
		div.setTextContent(m);
		dom.getElementById("body").appendChild(div);
		engine.executeScript("scroll()");
	
	}
	
	/**
	 * Execute the code
	 * @param code
	 * @return 
	 * @throws ScriptException 
	 */
	private Object execute(String code) throws ScriptException {
		echoCommand(code);
		bufferPointer = 0;
		if (!buffer.contains(code)) {
			buffer.add(0, code);			
			if (buffer.size() > MAX_INPUT_BUFFER_LENGTH) {
				buffer = buffer.subList(0, MAX_INPUT_BUFFER_LENGTH-1);
			}
		}
		Bindings b = script.getBindings();
		Set<String> globals = b.keySet();
		
		// TODO: fix this for basic types like Integer, String etc.
		//if (b.containsKey(code)) {
		//	ScriptObjectMirror mirror = (ScriptObjectMirror) b.get(code);
		//	System.out.println(mirror.keySet());
		//}
		
		return script.eval(code);
	}
	

	
}





